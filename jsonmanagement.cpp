#include "jsonmanagement.h"

#include <iostream>
#include <QFile>
#include <QString>

Jsn::JsnFile::JsnFile(const JsnFile& json) : schemaStructure()
{

}

Jsn::JsnFile::JsnFile(JsnFile&& json) noexcept : schemaStructure(std::move(json.schemaStructure))
{
    json.schemaStructure.reset();
}

Jsn::JsnFile& Jsn::JsnFile::operator=(const JsnFile& json)
{
    if (this == &json) return *this;
    schemaStructure = std::make_unique<SchemaElement>(*(json.schemaStructure));
    json.schemaStructure.reset();
    return *this;
}

Jsn::JsnFile& Jsn::JsnFile::operator=(JsnFile&& json) noexcept
{
    if (this == &json) return *this;
    schemaStructure = std::move(json.schemaStructure);
    json.schemaStructure.reset();
    return *this;
}

QString Jsn::JsnFile::ReadJsonToString(const char* filename)
{
    QFile file;
    file.setFileName(filename);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    if (!file.isOpen()) throw std::runtime_error("File not open! Cannot read .json file!");
    return QString {file.readAll()};
}

QString Jsn::JsnFile::ReadJsonToString(QString filename)
{
    return ReadJsonToString(filename.toStdString().c_str());
}

QJsonDocument Jsn::JsnFile::GetJsonDoc(const QString& json_string)
{
    return QJsonDocument::fromJson(json_string.toUtf8());
}

void Jsn::JsnFile::GetSchema(const QJsonObject& json_obj)
{
    if (schemaStructure == nullptr) throw std::runtime_error("While getting schema, schemaStructure cannot be a nullptr!");
    TraverseJsonObject(json_obj);
}

void Jsn::JsnFile::TraverseJsonObject(const QJsonObject &json_obj, int depth)
{
    for (int i = 0; i < depth; ++i) std::cout << "  ";
    std::cout << "{\n";
    for (QString& key : json_obj.keys())
    {
        if (json_obj[key].isObject()) TraverseJsonObject(json_obj[key].toObject(), depth+1);
        else HandleVariants(key, json_obj[key], JsonLocation::OBJECT, depth);
    }
    for (int i = 0; i < depth; ++i) std::cout << "  ";
    std::cout << "}\n";
}

void Jsn::JsnFile::HandleVariants(const QString& key, const QJsonValue& val, const JsonLocation& location, int depth)
{
    if (!val.isObject() && !val.isArray() && !val.isUndefined())
    {
        for (int i = 0; i < depth; ++i) std::cout << "  ";
        if (location == JsonLocation::OBJECT) std::cout << key.toStdString() << " : ";

        if (val.isString())
        {
            std::cout << val.toString().toStdString();
        }
        else if (val.isDouble())
        {
            std::cout << val.toDouble();
        }
        else if (val.isBool())
        {
            std::cout << val.toBool();
        }
        else throw JsnTypeException("Unknown type at " + std::to_string(depth) + " depth level.");
        std::cout << std::endl;
    }
    else if (val.isArray()) TraverseJsonArray(key, val.toArray(), depth+1);
    else if (val.isObject()) throw JsnTypeException("JsonObject where it should never occur.");
    else if (val.isUndefined()) throw JsnTypeException(std::string("Undefined type on ") + std::to_string(depth) + std::string(" depth level."));
    else throw JsnTypeException("Type unknown at " + std::to_string(depth) + " depth level.");
}

void Jsn::JsnFile::TraverseJsonArray(const QString& key, const QJsonArray &json_arr, int depth)
{
    for (int i = 0; i < depth-1; ++i) std::cout << "  ";
    std::cout << key.toStdString() << " : [\n";

    for (const QJsonValue& val : json_arr)
    {
        if (val.isObject()) TraverseJsonObject(val.toObject(), depth+1);
        else HandleVariants(key, val, JsonLocation::ARRAY, depth);
    }
    for (int i = 0; i < depth-1; ++i) std::cout << "  ";
    std::cout << "]\n";
}

void Jsn::JsnFile::ManageElementInJson(const QString& type, const QJsonObject& jsonObj, SchemaElementsPtrVec& schemaVector)
{
    /*
     if (type == "root") schemaVector.push_back(std::make_shared());
     else if (type == "tab");
     else if (type == "tree");
     else if (type == "schemaalternatives");
     else if (type == "double");
     else if (type == "int");
     else if (type == "enum");
     else if (type == "color");
     else throw std::runtime_error("Unknown Schema type! Cannot create SchemaElement");
    */
}

void Jsn::JsnFile::createSchemaElement(const QString& type, const QJsonObject& jsonObj, SchemaElementsPtrVec& schemaVector)
{
   /*
    if (type == "root") schemaVector.push_back(std::make_shared());
    else if (type == "tab");
    else if (type == "tree");
    else if (type == "schemaalternatives");
    else if (type == "double");
    else if (type == "int");
    else if (type == "enum");
    else if (type == "color");
    else throw std::runtime_error("Unknown Schema type! Cannot create SchemaElement");
   */
}
