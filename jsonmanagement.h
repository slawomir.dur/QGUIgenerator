#ifndef JSONMANAGEMENT_H
#define JSONMANAGEMENT_H

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>

#include <vector>
#include <memory>

typedef std::shared_ptr<SchemaElement*> SchemaElementsPtr;
typedef std::vector<SchemaElementsPtr> SchemaElementsPtrVec;

namespace Jsn
{
    class JsnFile
    {
    private:
        std::unique_ptr<SchemaElement*> schemaStructure;
    private:
        enum class JsonLocation { OBJECT, ARRAY };
        enum class SchemaType { ROOT, TAB, TREE, SCHEMAALTERNATIVES, ENUM, DOUBLE, INT, COLOR };
    public:
        class JsnTypeException : public std::exception
        {
        protected:
            std::string msg;
        public:
            explicit JsnTypeException(const char* message) : msg(message) {}
            explicit JsnTypeException(const std::string& message) : msg(message.c_str()) {}
            virtual ~JsnTypeException() {}
            virtual const char* what() const throw() override { return msg.c_str(); }
        };

    public:
        JsnFile() = default;
        JsnFile(const JsnFile&);
        JsnFile(JsnFile&&) noexcept;
        ~JsnFile() = default;
        JsnFile& operator=(const JsnFile&);
        JsnFile& operator=(JsnFile&&) noexcept;
        QString ReadJsonToString(const char* filename);
        QString ReadJsonToString(QString filename);
        QJsonDocument GetJsonDoc(const QString& json_string);
        void GetSchema(const QJsonObject& json_obj);
        void CreateSchemaStructure() { schemaStructure = std::make_shared<SchemaElement*>(SchemaElement); }
    private:
        void TraverseJsonObject(const QJsonObject &json_obj, int depth = 0);
        void HandleVariants(const QString& key, const QJsonValue& val, const JsonLocation& location, int depth);
        void TraverseJsonArray(const QString& key, const QJsonArray &json_arr, int depth);
        void ManageElementInJson(const QString& type, const QJsonObject& jsonObj, SchemaElementsPtrVec& schemaVector);
        void CreateSchemaElement(const QString& type, const QJsonObject& jsonObj, SchemaElementsPtrVec& schemaVector);
    };
}

#endif // JSONMANAGEMENT_H
