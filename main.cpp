#include <QApplication>
#include <QString>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>

#include <QVBoxLayout>
#include <QHBoxLayout>

#include <QDebug>
#include <QFile>
#include <QTextStream>

#include <QMainWindow>

#include <iostream>
#include <vector>
#include <algorithm>

#include "jsonmanagement.h"

int main(int argv, char** argc)
{
    QApplication app(argv, argc);

    QString json_string = Jsn::JsnFile::ReadJsonToString("../Rohr.json");
    QJsonObject json_obj = Jsn::JsnFile::GetJsonDoc(json_string).object();

    Jsn::JsnFile::GetSchema(json_obj);


    return app.exec();
}
