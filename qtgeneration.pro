TEMPLATE=app
TARGET=QtGeneration

QMAKE_CXXFLAGS += -std=c++11

QT=core gui

greaterThan(QT_MAJOR_VERSION, 4):QT+=widgets

SOURCES += \
    main.cpp \
    jsonmanagement.cpp

HEADERS += \
    jsonmanagement.h
